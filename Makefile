#!/usr/bin/make -f
CC:=gcc
CFLAGS_ALL:=-I/usr/include/libxml2 -I/usr/include/postgresql -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -Wall
LDFLAGS_ALL:=-lxml2 -lpq -lglib-2.0
CFLAGS_RELEASE:=-Os
CFLAGS_DEBUG:=-O0 -g -DDEBUG
LDFLAGS_RELEASE=-s
LDFLAGS_DEBUG=-g

.PHONY: default
default: release

.PHONY: debug
debug:
	$(MAKE) pgxmlimporter CFLAGS="$(CFLAGS_ALL) $(CFLAGS_DEBUG)" LDFLAGS="$(LDFLAGS_ALL) $(LDFLAGS_DEBUG)"

.PHONY: release
release:
	$(MAKE) pgxmlimporter CFLAGS="$(CFLAGS_ALL) $(CFLAGS_RELEASE)" LDFLAGS="$(LDFLAGS_ALL) $(LDFLAGS_RELEASE)"

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

pgxmlimporter: main.o
	$(CC) -o $@ $< $(LDFLAGS)

clean:
	rm -vf main.o pgxmlimporter
