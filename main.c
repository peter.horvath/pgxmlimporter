#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libxml/parser.h>
#include <libxml/xmlreader.h>
#include <libpq-fe.h>
#include <gmodule.h>

#ifdef DEBUG
#define debug(...) printf(__VA_ARGS__)
#else
#define debug(...)
#endif

void die(const char* type, const char* msg) {
  fprintf (stderr, "%s: %s\n", type, msg);
  exit (1);
}

gchar** g_slist_to_array(GSList *slist) {
  int len = g_slist_length(slist);
  gchar** rval = (char**)g_malloc(sizeof(char*)*(len+1));
  int p=0;
  while (slist) {
    rval[p] = slist->data;
    slist = slist->next;
    p++;
  }
  rval[len]=NULL;
  return rval;
}

gchar* pg_id(PGconn* conn, const gchar* str) {
  return PQescapeIdentifier(conn, str, strlen(str));
}

gchar* pg_literal(PGconn* conn, const gchar* str) {
  return PQescapeLiteral(conn, str, strlen(str));
}

PGresult* pg_query(PGconn *conn, const gchar* query) {
  debug ("\nquery: %s\n\n", query);
  PGresult *result = PQexec(conn, query);
  ExecStatusType status = PQresultStatus(result);
  if ((status != PGRES_COMMAND_OK) && (status != PGRES_TUPLES_OK)) {
    die (query, PQresultErrorMessage(result));
  }
  return result;
}

void pg_exec(PGconn *conn, const gchar* query) {
  PQclear(pg_query(conn, query));
}

gchar* pg_query_string(PGconn *conn, const gchar* query) {
  PGresult *result = pg_query(conn, query);
  gchar* rval = g_strdup(PQgetvalue(result, 0, 0));
  if ((PQntuples(result) != 1) || (PQnfields(result) != 1)) {
    die ("pg_query_string", "result should be 1x1");
  }
  PQclear(result);
  return rval;
}

static gchar* schema = "public";
static gchar* connDescr = "";
static gchar* input = NULL;

static GOptionEntry entries[] = {
  { "schema", 's', 0, G_OPTION_ARG_STRING, &schema, "Schema to use", "schema" },
  { "conn", 'c', 0, G_OPTION_ARG_STRING, &connDescr, "Connection descriptor as in PQconnectdb()", "connDescr" },
  { "input", 'i', 0, G_OPTION_ARG_STRING, &input, "Xml file dump, use \"-\" for stdin, default stdin", "input" },
  { NULL }
};

void processArgs(int argc, char** argv) {
  GError* error = NULL;
  GOptionContext* context;
  context = g_option_context_new("Imports m$sql xml dumps into PostgreSQL");
  g_option_context_add_main_entries(context, entries, NULL);
  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    die ("invalid options", error->message);
  }
}

int main(int argc, char** argv) {
  gchar* schemaId = NULL;
  gchar* schemaLiteral = NULL;
  gchar *tblName = NULL;

  int inputFd;

  int rowCount = 0;
  int firstNode = 1;

  processArgs(argc, argv);

  if (!input || !input[0] || !strcmp(input,"-")) {
    inputFd = STDIN_FILENO;
  } else {
    inputFd = open(input, O_RDONLY);
    if (inputFd == -1) {
      die ("can not open input file", strerror(errno));
    }
  }

  debug ("schema: %s connDescr: %s inputFd: %i\n", schema, connDescr, inputFd);

  PGconn *conn = PQconnectdb(connDescr);
  if (PQstatus(conn) != CONNECTION_OK) {
    die ("postgresql conn error", PQerrorMessage(conn));
  }
  pg_exec(conn, "BEGIN;");

  xmlTextReaderPtr reader = xmlReaderForFd(inputFd, NULL, "UTF-8", XML_PARSE_PEDANTIC);
  if (!reader) {
    die ("can not initialize xmlReaderForFd on input", strerror(errno));
  }

  while (1) {
    int nodeType;
    int ret = xmlTextReaderRead(reader);
    if (ret < 0) {
      die ("xml parse error", "");
    } else if (ret != 1) {
      break; // EOF
    }
    nodeType = xmlTextReaderNodeType(reader);
    if (nodeType != XML_READER_TYPE_ELEMENT) {
      continue;
    }
    if (firstNode) {
      debug ("firstNode\n");
      firstNode = 0;
      schemaId = pg_id(conn, schema);
      schemaLiteral = pg_literal(conn, schema);
      gchar *tblCiName = pg_literal(conn, (const gchar*)xmlTextReaderConstName(reader));
      gchar* tblNameQuery = g_strconcat("\
        SELECT table_name \
        FROM information_schema.tables \
        WHERE LOWER(table_schema)=LOWER(", schemaLiteral,") AND LOWER(table_name) = LOWER(", tblCiName, "); \
      ", NULL);
      PQfreemem(tblCiName);
      gchar *tblRawName = pg_query_string(conn, tblNameQuery);
      g_free(tblNameQuery);
      tblName = pg_id(conn, tblRawName);
      g_free(tblRawName);
      continue;
    } else {
      xmlTextReaderMoveToFirstAttribute(reader);
      GSList *names=NULL, *values=NULL;
      for (;;) {
        const gchar* name = (const gchar*)xmlTextReaderConstName(reader);
        if (!name)
          die ("xml error", "attribute name is NULL");
        gchar* name_pg = pg_id(conn, (const gchar*)name);

        const gchar* value =  (const gchar*)xmlTextReaderConstValue(reader);
        if (!value)
          die ("xml error", "attribute value is NULL");

        gchar* value_pg = pg_literal(conn, (const gchar*)value);

        names = g_slist_append(names, name_pg);
        values = g_slist_append(values, value_pg);
        if (xmlTextReaderMoveToNextAttribute(reader) != 1) {
          break;
        }
      }
      gchar** nameArr = g_slist_to_array(names);
      gchar** valueArr = g_slist_to_array(values);
      gchar* insertNames = g_strjoinv(",",nameArr);
      gchar* insertValues = g_strjoinv(",",valueArr);
      g_slist_free_full(names, PQfreemem);
      g_slist_free_full(values, PQfreemem);
      g_free(nameArr);
      g_free(valueArr);

      gchar* query = g_strconcat(" ", "INSERT INTO ", schemaId, ".", tblName, " (", insertNames, ") VALUES (", insertValues, ");", NULL);

      g_free(insertNames);
      g_free(insertValues);

      pg_exec(conn, query);
      rowCount++;
      g_free(query);
    }
  }

  pg_exec(conn, "COMMIT;");
  PQfinish(conn);

  PQfreemem(tblName);

  PQfreemem(schemaId);
  PQfreemem(schemaLiteral);

  xmlFreeTextReader(reader);

  if (inputFd != STDIN_FILENO) {
    close(inputFd);
  }

  printf ("%i rows imported.\n" , rowCount);
  return 0;
}
